// ==UserScript==
// @name         Discord Obliterate 'Nitro' Button
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Nukes the 'Nitro' button that has suddenly appeared around 30th Jan 2020. This is a new script because it has distinct behavior.
// @author       20kdc
// @match        https://discordapp.com/*
// @match        https://discord.com/*
// @grant        none
// ==/UserScript==

// I release this user-script into the public domain.

// Since iterating through the entire DOM would be performance suicide,
//  let's try to detect classes in ANY OTHER WAY.
var dragonequus;
dragonequus = {
    version: 4.2,
    getAllClassesLen: 0,
    getAllClassesCache: [],
    getAllClasses: function () {
        var sheets = document.styleSheets;
        if (sheets.length == dragonequus.getAllClassesLen) {
            return dragonequus.getAllClassesCache;
        }
        var workspace = [];
        var seen = {};
        for (var k = 0; k < sheets.length; k++) {
            var sheet = sheets[k];
            for (var k2 = 0; k2 < sheet.cssRules.length; k2++) {
                var rule = sheet.cssRules[k2];
                if (rule.type == CSSRule.STYLE_RULE) {
                    // .A:I .B:I, .A .B
                    var majors = rule.selectorText.split(",");
                    for (var k3 = 0; k3 < majors.length; k3++) {
                        var minors = majors[k3].split(" ");
                        for (var k4 = 0; k4 < minors.length; k4++) {
                            // Minor starts off as say .A:B
                            var minor = minors[k4];
                            // Must be class
                            if (!minor.startsWith("."))
                                continue;
                            // Cut off any : and remove .
                            var selectorBreak = minor.indexOf(":");
                            if (selectorBreak != -1) {
                                minor = minor.substring(1, selectorBreak);
                            } else {
                                minor = minor.substring(1);
                            }
                            if (seen[minor])
                                continue;
                            seen[minor] = true;
                            workspace.push(minor);
                        }
                    }
                }
            }
        }
        dragonequus.getAllClassesLen = sheets.length;
        dragonequus.getAllClassesCache = workspace;
        return workspace;
    },
    isValidDC: function (obfuscated, real) {
        if (!obfuscated.startsWith(real + "-"))
            return false;
        if (obfuscated.length != real.length + 7)
            return false;
        return true;
    },
    findAllByDiscordClass: function (name) {
        var q = [];
        var q2 = document.querySelectorAll("." + name);
        for (var k2 = 0; k2 < q2.length; k2++)
            q.push(q2[k2]);
        var classes = dragonequus.getAllClasses();
        for (var k in classes) {
            var n = classes[k];
            if (dragonequus.isValidDC(n, name)) {
                q2 = document.querySelectorAll("." + n);
                for (var k2 = 0; k2 < q2.length; k2++)
                    q.push(q2[k2]);
            }
        }
        return q;
    },
    findByDiscordClass: function (name) {
      	var all = dragonequus.findAllByDiscordClass(name);
      	if (all.length > 0)
            return all[0];
        return null;
    },
    toDiscordClass: function (name) {
        var classes = dragonequus.getAllClasses();
        for (var k in classes) {
            var n = classes[k];
            if (dragonequus.isValidDC(n, name))
                return n;
        }
        return name;
    },
    injectCSS: function (getCSS) {
        setTimeout(function () {
            var styleElm = document.createElement('style');
            var res = getCSS();
            console.log("dragonequus CSS: " + res);
            document.body.appendChild(styleElm);
            styleElm.sheet.insertRule(res, 0);
        }, 1000);
    },
    injectCSSForClassScript: function (clazz, css) {
        dragonequus.injectCSS(function () {
            return "." + dragonequus.toDiscordClass(clazz) + " { " + css + "}";
        });
    },
};

dragonequus.injectCSS(function () {
    return "a[href=\"/store\"] { display: none; }";
    // return "#private-channels-1 { display: none; }"; // could backfire
});

