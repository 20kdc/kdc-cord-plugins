#!/usr/bin/env python3

# BSD 3-Clause License
#
# Copyright (c) 2022- 20kdc <asdd2808@gmail.com> All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# ./saf2html.py raw.saf.json | pandoc -f html -t markdown_strict > processed.md

import json
import sys
import bs4
from bs4.element import Tag, NavigableString

blob = json.load(open(sys.argv[1], "rb"))

base_url = "https://discord.com/"

if "base_url" in blob:
    base_url = blob["base_url"]

delme_nodenames = {}
# delme_nodenames["TIME"] = True

style_nodenames = {}
style_nodenames["A"] = {"href": "href"}
style_nodenames["TIME"] = {"datetime": "datetime"}
style_nodenames["B"] = {}
style_nodenames["EM"] = {}
style_nodenames["U"] = {}
style_nodenames["S"] = {}
style_nodenames["STRONG"] = {}
style_nodenames["H3"] = {}

def tag(name):
	return Tag(name=name, is_xml=True)
def txt(t):
	return NavigableString(t)

def handle_tag_children(tag, para):
    for v2 in tag["children"]:
        handle_node(v2, para)

def handle_tag_children_spaced(tag, para):
    for v2 in tag["children"]:
        para.append(txt("\n"))
        handle_node(v2, para)

def fix_src(unfixed_src):
    wh_index = unfixed_src.find("&width=")
    if wh_index != -1:
        unfixed_src = unfixed_src[:wh_index]
    if unfixed_src.startswith("/"):
        return base_url + unfixed_src[1:]
    else:
        return unfixed_src

queued_reply = None

def handle_node(node, para):
    global delme_nodenames, style_nodenames, base_url, queued_reply
    handle_queued_reply = False
    if node["type"] == "element":
        nname = node["tagName"]
        nattrs = node["attributes"]
        # hide avatars
        if "class" in nattrs:
            if nattrs["class"].find("avatar_") != -1:
                return
            if nattrs["class"].find("replyAvatar_") != -1:
                return
            if nattrs["class"].find("repliedMessage_") != -1:
                replybasis = tag("span")
                replybasis.append(txt(" (in reply to:"))
                handle_tag_children_spaced(node, replybasis)
                replybasis.append(txt(")"))
                queued_reply = replybasis
                return
        # continue
        if nname == "IMG":
            text = "IMG?"
            if "src" in nattrs:
                unfixed_src = nattrs["src"]
                text = unfixed_src
                t = tag("a")
                t.attrs["href"] = fix_src(unfixed_src)
                alt_read = ""
                if "alt" in nattrs:
                    alt_read = nattrs["alt"]
                if alt_read != "":
                    text = alt_read
                t.append(txt(text))
                para.append(t)
            elif "alt" in nattrs:
                para.append(txt(nattrs["alt"]))
            return
        if nname in delme_nodenames:
            # nope!
            return
        if nname in style_nodenames:
            # stylistically important
            template = style_nodenames[nname]
            innards = tag(nname)
            for k in template.keys():
                if k in nattrs:
                    innards.attrs[template[k]] = nattrs[k]
            handle_tag_children(node, innards)
            para.append(innards)
            if nname == "H3":
                if not (queued_reply is None):
                    innards.append(queued_reply)
                    queued_reply = None
            return
    elif node["type"] == "text":
        the_text = node["text"]
        while True:
            newline_loc = the_text.find("\n")
            if newline_loc == -1:
                break
            text_pre_nl = the_text[:newline_loc]
            if text_pre_nl != "":
                para.append(txt(text_pre_nl))
            para.append(tag("br"))
            the_text = the_text[newline_loc + 1:]
        if the_text != "":
            para.append(txt(the_text))
    handle_tag_children(node, para)

for msg in blob["messages"]:
    para = tag("p")
    handle_node(msg["html"], para)
    print(para.encode().decode("utf8"))

