class_name SAFHMessage
extends Resource

export var id: String = ""
export var prev_id: String = ""
export var channel: String = ""
export var text: String = ""
export var html: Dictionary

func to_dict():
	var res := {}
	if id != "":
		res["id"] = id
	if prev_id != "":
		res["prevId"] = prev_id
	res["channel"] = channel
	res["text"] = text
	res["html"] = html
	return res
