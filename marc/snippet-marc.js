/*
BSD 3-Clause License

Copyright (c) 2022- 20kdc <asdd2808@gmail.com> All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors may
   be used to endorse or promote products derived from this software

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

/*
 * Message Archival Script - THIS IS NOT A USERSCRIPT.
 * YOU ARE EXPECTED TO KNOW WHAT YOU'RE DOING IF YOU USE THIS.
 */

// Classes (YOU MUST SET THESE MANUALLY)
let marcClassMLI = "messageListItem__050f9";

function marcXMLJSON(v) {
    var node = {};
    if (v instanceof Element) {
        node.type = "element";
        node.tagName = v.tagName;
        node.attributes = {};
        for (let attr of v.attributes) {
            if (attr.name == "aria-label") {
                // block certain stuff from reaching archival if we're SURE we'll never care about it
                if (attr.textContent == "Message Actions")
                    return null;
            }
            node.attributes[attr.name] = attr.textContent;
        }
    } else if (v instanceof Text) {
        node.type = "text";
        node.text = v.textContent;
    } else {
        return null;
    }
    node.children = [];
    for (let sv of v.childNodes) {
        let res = marcXMLJSON(sv);
        if (res !== null)
            node.children.push(res);
    }
    return node;
}

// use Copy Object to get this out
function marcScan() {
    var items = document.getElementsByClassName(marcClassMLI);
    if (items.length == 0)
        throw new Error("Freeman, you fool!");
    var messages = [];
    var prevId = null;
    for (let v of items) {
        let msg = {};
        // id stuff
        let discordCID = v.id.split("-")[2];
        let discordMID = v.id.split("-")[3];
        msg.id = "discord/" + discordMID;
        if (prevId !== null)
            msg.prevId = prevId;
        prevId = msg.id;
        msg.channel = "discord/" + discordCID;
        msg.text = v.outerText;
        msg.html = marcXMLJSON(v);
        messages.push(msg);
    }
    var data = {
        type: "session archive file (html)",
        href: location.href,
        messages: messages
    };
    return data;
}

