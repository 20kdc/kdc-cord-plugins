extends VBoxContainer

onready var label_segment_count := $"%segment_count"
onready var messages_view := $"%messages_view"
onready var asp := $AudioStreamPlayer
onready var export_fn := $"%export_fn"

var messages = {}

var segments = 0
var href_last = ""

func _ready():
	label_segment_count.text = "0"

func _on_importer_input_text_changed():
	_importer_receive($HBoxContainer/importer_input.text)

func _importer_receive(text):
	messages_view.scroll_following = true
	asp.stop()
	if _on_importer_online_received_from_clipboard_detail(text):
		asp.stream = preload("confirm.wav")
	else:
		asp.stream = preload("fail.wav")
	asp.play()
	rebuild_msg_ui()

func get_msg(id: String) -> SAFHMessage:
	return messages[id]

func _on_importer_online_received_from_clipboard_detail(text):
	var pres := JSON.parse(text)
	if pres.error != OK:
		print(pres.error_string)
		return false
	if not (pres.result is Dictionary):
		return false
	var res: Dictionary = pres.result
	if not res.has("type"):
		print("no type")
		return false
	if res["type"] != "session archive file (html)":
		print("type is not session archive file")
		return false
	print("Confirmed valid segment: " + str(len(res["messages"])) + " messages")
	segments += 1
	label_segment_count.text = str(segments)
	href_last = res["href"]
	# alright, no more paranoid checks from here on out
	for v in res["messages"]:
		var msgd: Dictionary = v
		var msg := SAFHMessage.new()
		msg.id = msgd["id"]
		if msgd.has("prevId"):
			msg.prev_id = msgd["prevId"]
		msg.channel = msgd["channel"]
		msg.text = msgd["text"]
		msg.html = msgd["html"]
		if messages.has(msg.id):
			# overwrite only if this message has a previous ID and the old one doesn't
			if msg.prev_id != "":
				if messages[msg.id].prev_id == "":
					messages[msg.id] = msg
		else:
			messages[msg.id] = msg
	return true

func make_message_key_list():
	var unadded_keys: Array = messages.keys()
	var final_keys := []
	var next_prev_id := ""
	while len(unadded_keys) > 0:
		var res_key = ""
		# three passes based on desperation
		if res_key == "":
			for potential_key in unadded_keys:
				var msg := get_msg(potential_key)
				if msg.prev_id == next_prev_id:
					res_key = potential_key
					break
		if res_key == "":
			for potential_key in unadded_keys:
				var msg := get_msg(potential_key)
				if msg.prev_id == "":
					res_key = potential_key
					break
		if res_key == "":
			for potential_key in unadded_keys:
				res_key = potential_key
				break
		if res_key == "":
			print("ran out of keys despite the fact that shouldn't even happen")
			return final_keys
		final_keys.push_back(res_key)
		unadded_keys.erase(res_key)
	return final_keys

func rebuild_msg_ui():
	messages_view.clear()
	for k in make_message_key_list():
		var msg := get_msg(k)
		var txp := msg.text
		# workarounds
		if txp.begins_with("[\n"):
			txp = txp.substr(2)
			txp = txp.replace("\n]\n", "\n")
		txp = txp.replace(" — \n", "")
		messages_view.add_text(txp + "\n\n")

func _on_delete_button_pressed():
	messages_view.scroll_following = true
	messages = {}
	rebuild_msg_ui()
	segments = 0

func _on_export_button_pressed():
	# main writing loop
	var messages2 = []
	for k in make_message_key_list():
		var msg := get_msg(k)
		messages2.push_back(msg.to_dict())
	# done, finish up
	var thefile = {"type": "session archive file (html)", "href": href_last}
	thefile["messages"] = messages2

	var text := JSON.print(thefile)
	var f := File.new()
	f.open(export_fn.text, File.WRITE)
	f.store_buffer(text.to_utf8())
	f.close()
	asp.stop()
	asp.stream = preload("confirm.wav")
	asp.play()

func _on_messages_view_meta_clicked(meta):
	if meta[0] == "delete":
		messages_view.scroll_following = false
		messages.erase(meta[1])
		rebuild_msg_ui()

func _on_delete_button3_pressed():
	messages_view.scroll_following = false
	var mkl = make_message_key_list()
	if len(mkl) == 0:
		return
	messages.erase(mkl[0])
	rebuild_msg_ui()

func _on_delete_button2_pressed():
	messages_view.scroll_following = true
	var mkl = make_message_key_list()
	if len(mkl) == 0:
		return
	messages.erase(mkl[len(mkl) - 1])
	rebuild_msg_ui()

func _on_import_button_pressed():
	asp.stop()
	asp.stream = preload("fail.wav")
	var f := File.new()
	if f.open(export_fn.text, File.READ) == OK:
		if _on_importer_online_received_from_clipboard_detail(f.get_as_text(false)):
			asp.stream = preload("confirm.wav")
		f.close()
	asp.play()
	rebuild_msg_ui()
