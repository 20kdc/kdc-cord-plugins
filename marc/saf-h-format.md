# Session Archive Format, HTML Variant

This is the revised, refactored and improved session archive format.

This version of the format leans hard into archiving everything so that it can then be processed at a later date.

As such, SAF-H has a "model" of the kinds of software that will use it.

* Archiving: This software is responsible for raw data input. It's the ultimate creator of SAF-H files. (This is the `snippet-marc.js` code in the reference stack.)
* Collation: This software is responsible for merging SAF-H files into a final archive and removing irrelevant messages. (This is the M-ARC Godot application in the reference stack.)
* Export / View: This software is responsible for displaying SAF-H files or converting them for display. (This is `saf2html.py` in the reference stack.)

## SAF XML-As-JSON

SAF XML-As-JSON is a format devised to avoid any absolute need to include an HTML parser *and* a JSON parser at the same time, in favour of only using JSON.

In practice, this will be "messed with" somewhat by other SAF variants (SAF-M, which will contain embedded HTML, will likely have to be converted into SAF-H to create output files).

The basis of XML-As-JSON is the Node. Nodes have a `"type"` field (string) and a `"children"` field (array of Nodes).

`"element"`-type nodes have a `"tagName"` field and an `"attributes"` (string to string dictionary) field.

Tag names are always in uppercase.

`"text"`-type nodes have a `"text"` field.

As far as I'm aware, this represents everything that will be relevant to archive for the purposes of SAF-H.

## SAFHFile

`SAFHFile` is a JSON Object. Rough example:

```
{
"type": "session archive file (html)",
"href": "https://example.com/blah/blah",
"messages": []
}
```

Note that `href` should be set to the browser's `location.href` field during capture.

`"messages"` must be an array of Messages *in presentation order*.

## Message

`Message` is another kind of JSON Object.

```
{
"id": "123X",
"prevId": "121Y",
"channel": "",
"text": "Example",
"html": {...}
}
```

`"id"` is an (ideally globally) unique identifier for the message. This field is theoretically optional, because it's not certain that IDs are always available. However some software may require IDs, in particular collation software that does not provide another way to associate messages.

`"prevId"` is an optional ID for the previous message in the same channel.

Collation software tracking IDs should only overwrite if a `prevId` is in the message that is being written. (This is so all messages eventually have `prevId`s.)

*Important:* If `id` is Archiver-provided, IDs mustn't be *changed* (though it's okay to merge messages from different sessions, each message has verbatim IDs). However, if a collator doesn't see IDs, it's fine to provide an interface to synchronize the streams manually and generate custom IDs. Prefix these with `custom/` please.

`"channel"` is a channel ID of some kind. This must be present, but can be empty if there is no channel ID. (This has to be present so that there is a "theoretical channel".)

`"text"` is non-authoritative, but shouldn't be modified if it can be avoided. It's the Archiver's guess at a presentable plain-text view of the message, and can be used by Collation software accordingly.

`"html"` is the HTML of the message's root node, in XML-As-JSON format. If necessary a root may be synthesized to hold all the message parts.

## Orderings

Presentation order is "whatever the exporting application defines it as", but it's important to note this because it implies some intent behind the order that other applications should respect.

Chain order is a non-deterministic order defined by `make_message_key_list` in M-ARC, used to create an initial presentation order based on ID chains.

In short:

- At the start, it takes the first found message with an empty `prevId`.
- Once there's a message, the next message is the first found message with `prevId` equal to the current `id`.
- If that fails, the next message is the first found message with an empty `prevId`.
- If that fails, the next message is the first found message, period.

This naturally separates messages by channel and keeps coherence across different joined recordings.

## Semantics

An inherent and somewhat unfortunate property of SAF-H is that Export / View software requires specific tuning for the specific chat services that will be involved, and their specific versions.

With that in mind, this section is intentionally left undefined in the broader case.

If translation to SAF-H is performed, then specifications will be expected for defining the semantics of their various IDs and message HTML.

However, in the general case, the semantics cannot be pinned down as pinning them would involve the cooperation of external uncooperative entities.

