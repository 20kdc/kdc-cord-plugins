# kdc-cord-plugins

There used to be plugins here using [a proper Discord client mod](https://github.com/Cumcord). Now there isn't. That client mod has been unusable for quite a while.

Still. If any developers of said client mod happen to read this... thank you. The plugin I ended up writing was to censor what I received during a time when I couldn't really handle exposure to the information that was visible everywhere. It did bad things to me, and the plugin -- and by extension your client mod -- helped keep me from just shutting everything off. ~ K.

If you want to look at the plugins anyway, check commit `bc436ab90b3e4ab2a91f7a319cbf6ff7bf4d4be7`.

## Stuff In This Repository

* `marc`: Message archival tool for long logs.
* `userscripts`: Various userscripts. These tend to get updated and mirrored to Greasy Fork whenever I notice my setup breaking.
